const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema(
	{
		userId: {
			type: String,
			required: true
		}, 
		orders: [
			{
				products: [
				 	{
				 	productName: {
				 		type: String,
				 		required: [true, "Product Name is required."]
				 	},
				 	productId: {
				 		type: String
				 	},
				 	quantity: {
				 		type: Number,
					 	required: [true, "Quantity is required."]
				 	},
				 	isActive: {
		 				type: Boolean,
		 				default: true
		 			}
				 }	 					
				], 
				totalAmount: {
					type: Number,
					required: [true, "Total amount is required."]
				},
				purchasedOn: {
					type: Date,
					default: new Date()
				}
			}
		]
	}
)

module.exports = mongoose.model("Order", orderSchema);

const jwt = require("jsonwebtoken");

// Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined secret keyword
const secret = "coursebookingAPI"

// [Section] JSON Web Tokens
		/*
		- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
		- Information is kept secure through the use of the secret code
		- Only the system that knows the secret code that can decode the encrypted information
		- Imagine JWT as a gift wrapping service that secures the gift with a lock
		- Only the person who knows the secret code can open the lock
		- And if the wrapper has been tampered with, JWT also recognizes this and disregards the gift
		- This ensures that the data is secure from the sender to the receiver
		*/

	// Token creation
	/*
	- Analogy
		Pack the gift and provide a lock with the secret code as the key
	*/

module.exports.createAccessToken = (user) => {
	// The data that will be received from the registration
	// When the user logs in, a token will be created with the user's information
	const data ={
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
	}
	// generates the token using the data and the secret code with no additional options provided
	return jwt.sign(data, secret, {});
}

// Token  Verification
/*
	Analogy
	- Receive the gift and open the lock to verify if the sender is legitimate and the gift was not tampered
*/
module.exports.verify = (req, res, next) => {

	// The token is retrieved from the request header
	let token = req.headers.authorization;

	// Token received is not undefined
	if (token !== undefined) {
		console.log(token);

		token = token.slice(7, token.length);

		// Validates the token using the "verify" method decrypting the token using the secret code
		return jwt.verify(token, secret, (err, data) =>{

			// If JWT is not valid
			if(err){
				return res.send({auth:"failed"});
			// If JWT is valid
			} else {
				// Allows the application to proceed to the next middleware/function in the route
				next();
			}
		}
		)

	// If token does not exist
	} else {
		return res.send({auth: "failed"})
	}
}

// Token decryption
/*
	Analogy - Open the gift and get the content
*/
module.exports.decode = (token) => {
	if (token !== undefined) {

		token = token.slice(7, token.length);
		return jwt.verify(token, secret,(err, data) => {
			if (err) {
				return null;
			} else {
				// The "decode" method is used to obtain the information from the JWT
				// The "complete:true" option allows us to return additional information from the JWT token
				// Return an object with access to the payload which contains user information stored when the token was generated
				console.log(jwt.decode(token, {complete: true}).payload)
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	} else {
		return null;
	}
}
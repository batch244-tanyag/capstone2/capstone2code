const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes"); 
const orderRoutes = require("./routes/orderRoutes");

const app = express();

mongoose.connect("mongodb+srv://Admin:Admin@zuitt-course-booking.eaiaz8d.mongodb.net/E-commerce?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});



mongoose.connection.once('open', () =>
	console.log('Now connected to the MongoDB Atlas.')
);

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);



app.listen(process.env.PORT || 4000, () =>{
	console.log(`API is now online at port ${process.env.PORT || 4000}.`);
});
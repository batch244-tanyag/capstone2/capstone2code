const Product = require("../models/Product");

// Creating a product
module.exports.addProduct = (data) => {
	let newProduct = new Product ({
		name: data.product.name,
		description: data.product.description,
		price: data.product.price
	});

	return Product.findOne({name: data.product.name}).then(result => {
		if (result == null) {

			return newProduct.save().then((product, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			})

		} else {
			return false
		}
	})	
};


// Retrieve all products

module.exports.getAllProducts = () => {

	return Product.find({}).then(result => {
		return result;
	})
}


// Retrieving all active products
module.exports.getAllActive = () => {

	return Product.find({isActive:true}).then(result => {

		return result;
	})
};


// Retrieving a single product
module.exports.getProduct = (reqParams) => {
	
	return Product.findById(reqParams.productId).then(result => {

		return result;
	})
};


// Updating a product
module.exports.updateProduct = (reqParams, reqBody) => {
	

	return Product.findByIdAndUpdate(reqParams.productId, reqBody).then((product, error) => {

		if (error) {
			return false

		} else {
			return true
		}
	})
};


// Archiving a product
module.exports.archiveProduct = (reqParams, reqBody) => {

	let updateActiveField = {
		isActive : reqBody.isActive
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		if(error){
			return false
		} else {
			return true
		}
	})
};




const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
const Product = require("../models/Product");


// Check email if registered
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {

		// The find method returns a record if a match is found
		if (result.length > 0) {
			return true;

		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return false;
		}
	})
};

// User registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return User.findOne({email: reqBody.email}).then(result => {

		if (reqBody.email == "" || reqBody.password == "") {
			return ("Email or password must not be empty.")
		}

		if (result == null) {

			return newUser.save().then((user, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			})

		} else {
			return false
		}
	})	
};


// User authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}

			} else {
				return false
			}
		}
	})
};


// Non-admin user checkout(create order)

module.exports.checkout = async (data) => {

	let isProductUpdated = await Product.findById(data.productId);
	isProductUpdated.orders.push({userId: data.userId});

	let isUserUpdated = await User.findById(data.userId);

	let totalAmount = isProductUpdated.price * data.quantity;

	const orderedProduct = {
            products: [
            	{
	                productName : isProductUpdated.name,
	                quantity : data.quantity,
	        	}
 			],
            totalAmount : totalAmount
        };

    isUserUpdated.orders.push(orderedProduct);

    console.log(isUserUpdated)
    console.log(isProductUpdated)
	
	if (isUserUpdated && isProductUpdated && data.quantity > 0) {
		isProductUpdated.save();
		isUserUpdated.save();
		return true
	} else {
		return false
	}
};	


// Retrieve user details
module.exports.getProfile = (reqBody) => {

	return User.findById(reqBody.userId).then(result => {

		result.password = "";

		return result;
	})
};


/*STRETCH GOALS*/
// Set user as admin
module.exports.setAdmin = (reqParams) => {

	return User.findByIdAndUpdate(reqParams.userId, {isAdmin:true}).then((user, error) => {

		if(error){
			return false
			
		} else {
			return true
		}
	})
};


// Retrieve authenticated user's orders
module.exports.getMyOrders = (reqBody) => {

	return User.findById(reqBody.userId).then(user => {

		return user.orders;
	})
};


// Retriebe all orders
module.exports.getAllOrders = () => {
	
	return User.find({isAdmin:false}).then((user) => {

		let allOrders = []
		user.forEach(user => {
			allOrders.push(user.orders)
		})

		return allOrders;
	})
};
const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");


router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send (resultFromController));
});

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for user checkout
router.post("/checkout", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	let data = {
		userId : userData.id,
		productId : req.body.productId,
		quantity: req.body.quantity
	};


	if (userData.isAdmin == false){		

	userController.checkout(data).then(resultFromController => res.send(resultFromController));

	} else {
		res.send (false)
	}
});


// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});


/*STRETCH GOALS*/
// Route for setting user as admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);

	if (data.isAdmin) {
		userController.setAdmin(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send ("User not authorized")
	}
});


// Route for retrieving authenticated user's orders
router.get("/myOrders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin == false){
		userController.getMyOrders({userId : userData.id}).then(resultFromController => res.send(resultFromController));
	} else {
		res.send ("User not authorized")
	}
});


// Route for retrieving all orders
router.get("/allOrders", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);

	if (data.isAdmin) {
		userController.getAllOrders().then(resultFromController => res.send(resultFromController));
	} else {
		res.send("User not authorized")
	}
});


module.exports = router;
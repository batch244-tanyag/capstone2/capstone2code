const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth")

// Route for creating a product
router.post("/create", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin){
		productController.addProduct(data).then(resultFromController => res.send(resultFromController));

	} else {
		res.send ("User unauthorized to add product")
	}
});


// Retrieving all products
router.get("/allProducts", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving all active products
router.get("/active", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving a single product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
}) ;


// Route for updating a product
router.put("/:productId", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);

	if (data.isAdmin) {
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send ("User unauthorized to update product")
	}
})

// Route to archive a product
router.patch("/:productId/archive", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);

	if (data.isAdmin) {
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send ("User not authorized")
	}
});



module.exports = router;